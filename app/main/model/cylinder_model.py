from .. import db

class Cylinder(db.Model):
    """DB model for cylinder case"""

    __tablename__ = "cylinder"

    id = db.Column(db.Integer, autoincrement=True, nullable=False, unique=True)
    owner_name = db.Column(db.String(255), nullable=False)
    cylinder_type = db.Column(db.String(50), nullable=True)
    quantity = db.Column(db.Integer, nullable=False)
    condition = db.Column(db.String(20), nullable=False)
    city = db.Column(db.String(50), nullable=False)
    pincode = db.Column(db.Integer, nullable=False)
    state = db.Column(db.String(50), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    modified_on = db.Column(db.DateTime, nullable=False)
    cylinder_id = db.Column(db.String(16), primary_key=True, nullable=False)
    phone = db.Column(db.String(15), nullable=False, unique=True)
    passcode = db.Column(db.String(36), nullable=False)
    status = db.Column(db.Boolean, nullable=False)

class CylinderRequest(db.Model):
    """DB model for cylinder requests"""

    __tablename__ = "cylinder_request"

    id = db.Column(db.Integer, autoincrement=True, nullable=False, unique=True)
    rp_name = db.Column(db.String(255), nullable=False)
    rp_quantity = db.Column(db.Integer, nullable=False)
    rp_city = db.Column(db.String(50), nullable=False)
    rp_pincode = db.Column(db.Integer, nullable=False)
    rp_state = db.Column(db.String(50), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    modified_on = db.Column(db.DateTime, nullable=False)
    request_id = db.Column(db.String(16), primary_key=True, nullable=False)
    cylinder_id = db.Column(db.String(16), nullable=False)
    rp_phone = db.Column(db.String(15), nullable=False, unique=True)
    status = db.Column(db.Boolean, nullable=False)
    comment = db.Column(db.String(255), nullable=True)
