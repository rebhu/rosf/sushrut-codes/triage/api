# service module for business related operations

import uuid
import datetime as dt
import re

from flask import current_app as app
from ..model.cylinder_model import CylinderRequest
from ..support.cylinder_support import verify_owner
from ..support.cylinder_request_support import fetch_requests, fetch_request_number
from ..util.validation_schema import validate_data, escape_char
from ..util.error_responses import *
from app.main import db
# ---------------------------------------------------------------------
#                           CYLINDER HANDLING
# ---------------------------------------------------------------------
def add_cylinder_request(data):

    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    # if validated then register --------------------------------------
    request_id = str(uuid.uuid4())
    
    try:
        cylinder_request_data = CylinderRequest(
                    rp_name = data['rp_name'],
                    rp_quantity = data['rp_quantity'],
                    rp_city = data['rp_city'],
                    rp_pincode = int(data['rp_pincode']),
                    rp_state = data['rp_state'],
                    created_on = dt.datetime.utcnow(),
                    modified_on = dt.datetime.utcnow(),
                    request_id = db.func.UUID_TO_BIN(request_id),
                    cylinder_id = db.func.UUID_TO_BIN(data['cylinder_id']),
                    rp_phone = data['rp_phone'],
                    status = 1,
                    comment = data['comment']
                    )

        save_changes(cylinder_request_data)
        data['request_id'] = request_id

        return data, 200

    except Exception as err:
        
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return error_response_403(message)


# --------------- GET CYLINDER REQUESTS ---------------
def get_cylinder_request(data):
     # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])
    
    # verify cylinder owner
    verification_response = verify_owner(cylinder_id=data['cylinder_id'], phone=data['phone'], passcode=data['passcode'])
    if not verification_response[0]:
        return error_response_403(verification_response[1])

    keys = ['request_id', 'rp_name', 'created_on', 'modified_on', 
                'rp_quantity', 'rp_city', 'rp_pincode', 'rp_state', 'rp_phone']
    request_resp = fetch_requests(n=20, cylinder_id=data['cylinder_id'], cylinder_request_keys=keys)

    if not request_resp[0]:
        return error_response_403(request_resp[1])

    return request_resp[1], 200

# ------------- GET NUMBER OF REQUESTS -----------
def get_request_number(cylinder_id):
    number_resp = fetch_request_number(cylinder_id)

    if not number_resp[0]:
        return error_response_403(number_resp[1])
    
    return number_resp[1], 200

# support functions
def save_changes(data):
    db.session.add(data)
    db.session.commit()