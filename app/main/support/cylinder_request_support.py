import re
import datetime as dt
from hashlib import md5
import uuid

from app.main import db
from ..model.cylinder_model import CylinderRequest


def fetch_requests(n, cylinder_id, cylinder_request_keys):
    
    try:
        all_requests = CylinderRequest.query.filter_by(cylinder_id=db.func.UUID_TO_BIN(cylinder_id)).filter_by(status=1).order_by(db.desc(CylinderRequest.modified_on)).limit(n).all()
        
    except Exception as err:

        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    all_dict = []
    
    for a_request in all_requests:
        a_request_dict = a_request.__dict__
        
        request_dict = dict()    
        for key in cylinder_request_keys:
            if key == 'request_id':
                request_dict[key] = str(B_U(a_request_dict[key]))
            else: 
                request_dict[key] = str(a_request_dict[key])
        
        all_dict.append(request_dict)

    return True, all_dict


def fetch_request_number(cylinder_id):
    try:
        num_resp = CylinderRequest.query.filter_by(cylinder_id=db.func.UUID_TO_BIN(cylinder_id)).count()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message

    return True, num_resp


# support functions
def B_U(binary_uuid):
    return uuid.UUID(bytes=binary_uuid)