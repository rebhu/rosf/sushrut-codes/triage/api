from marshmallow import Schema, fields
from marshmallow.validate import Length, OneOf, ValidationError, Range, Equal
from flask import escape

# schema for form data validation
class valSchema(Schema):
    # # user
    # name = fields.Str(required=True, validate=Length(min=1,max=50))
    # email = fields.Email(required=True, validate=Length(min=5,max=50))
    # password = fields.Str(required=True, validate=Length(min=5,max=50))
    # role = fields.Str(required=True)

    # cylinder
    owner_name = fields.Str(required=True, validate=Length(min=1,max=50))
    cylinder_type = fields.Str(required=False, validate=Length(min=0,max=50))
    quantity = fields.Integer(required=True, validate=Range(min=1,max=10))
    condition = fields.String(required=True, validate=OneOf(['Filled', 'Partially filled', 'Empty', 'Damaged']))
    city = fields.Str(required=True, validate=Length(min=1,max=50))
    pincode = fields.Integer(required=True, validate=Range(min=0,max=999999))
    state = fields.Str(required=True, validate=Length(min=0,max=50))
    phone = fields.Str(required=True, validate=Length(min=1,max=15))
    passcode = fields.Str(required=True, validate=Length(equal=6))
    cylinder_id = fields.Str(required=True, validate=Length(equal=36))

    # page
    page = fields.Integer(required=True, validate=Range(min=1, max=50))

    #requests
    rp_name = fields.Str(required=True, validate=Length(min=1,max=50))
    rp_quantity = fields.Integer(required=True, validate=Range(min=1,max=10))
    rp_city = fields.Str(required=True, validate=Length(min=1,max=50))
    rp_pincode = fields.Integer(required=True, validate=Range(min=0,max=999999))
    rp_state = fields.Str(required=True, validate=Length(min=0,max=50))
    rp_phone = fields.Str(required=True, validate=Length(min=1,max=15))
    comment = fields.Str(required=False, validate=Length(min=1,max=500))


# validator
def validate_data(data, validatorList):
    # validate data
    try:
        ValidatorSchema = valSchema(only=validatorList)
    except ValueError:
        return False, 'Invalid field'
    try:
        ValidatorSchema.load(data)
        return True, {}
    
    except ValidationError as err:
        errorType = list(err.messages.keys())[0]
        errorMsg = errorType + ', ' + err.messages[errorType][0]

        return False, errorMsg

def escape_char(data_dict):
    """Function to escape special characters"""
    return {key: str(escape(data_dict[key])) for key in data_dict.keys()}