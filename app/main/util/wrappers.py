# wrappers for token operations
from functools import wraps

from flask import request, Response
from ..service.keycloak_pipeline import keycloakClientAccess, KY_credentials
from ..util.error_responses import *
from ..util.validation_schema import escape_char, validate_data

# token required wrapper
def token_required(f):
    @wraps(f)
    def check_token(*args, **kwargs):
        # check for cookie in header
        if not 'Cookie' in request.headers:
            return error_response_400("No cookies found")
            
        if not '_access_tkn' in request.cookies:
            return error_response_400("Access token missing")

        access_token = request.cookies['_access_tkn']

        keycloakOpenid = keycloakClientAccess(list(map(KY_credentials().__getitem__, [0,1,4,5])))

        tokenResp = keycloakOpenid.verify_token(access_token)
        
        if not tokenResp['resp']:
            return error_response_403(tokenResp['message'])

        if not tokenResp['token_info']['active']:
            return error_response_401("Invalid access token")
        
        user_info = {
            'email': tokenResp['token_info']['email'],
            'name': tokenResp['token_info']['name'],
            'user_id': tokenResp['token_info']['sub'],
            'role': tokenResp['token_info']['role'],
        }

        return f(user_info, **kwargs)

    return check_token


# # get access token wrapper
# def get_access_token(f):
#     @wraps(f)
#     def get_token(*args, **kwargs):
#         # check for cookie in header
#         if not 'Cookie' in request.headers:
#             return error_response_400("No cookies found")
            
#         if not '_refresh_tkn' in request.cookies:
#             return error_response_400("Refresh token missing")

#         refresh_token = request.cookies['_refresh_tkn']

#         keycloakOpenid = keycloakClientAccess(list(map(KY_credentials().__getitem__, [0,1,4,5])))

#         tokenResp = keycloakOpenid.get_token(refresh_token)

#         if not tokenResp['resp']:
#             return error_response_403(tokenResp['message'])

#         return f(tokenResp['token'], **kwargs)

#     return get_token

# delete token wrapper
def logout(f):
    @wraps(f)
    def del_token(*args, **kwargs):
        # check for cookie in header
        if 'Cookie' in request.headers:
            
            if  '_refresh_tkn' in request.cookies:
                
                refresh_token = request.cookies['_refresh_tkn']

                keycloakOpenid = keycloakClientAccess(list(map(KY_credentials().__getitem__, [0,1,4,5])))

                tokenResp = keycloakOpenid.delete_token(refresh_token)
                
        return f(*args, **kwargs)

    return del_token