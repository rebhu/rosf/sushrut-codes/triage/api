from flask_restplus import Namespace, fields

class cylinderDto:
    api = Namespace('cylinder', description='cylinder related operations')

    # Cylinder
    cylinder = api.model('cylinder', {
        'owner_name': fields.String(required=True, description="Name of the cylinder owner"),
        'cylinder_type': fields.String(required=False, description="Type of cylinder"),
        'quantity': fields.Integer(required=True, description="Number of cylinders"),
        'condition': fields.String(required=True, description='Condition of cylinder'),
        'city': fields.String(required=True, description="City of cylinder owner"),
        'pincode': fields.Integer(required=True, description="pincode of cylinder owner"),
        'state': fields.String(required=True, description="State of cylinder owner"),
        'phone': fields.String(required=True, description="POC of owner")
        
    })  

    # Cylinder Update
    cylinder_update = api.model('cylinder_update', {
        'phone': fields.String(required=True, description="POC of owner"),
        'passcode': fields.String(required=True, description="passcode")
        
    })    


    # Cylinder Requests
    cylinder_request = api.model('cylinder_request', {
        'rp_name': fields.String(required=True, description='Request person name'),
        'rp_quantity': fields.Integer(required=True, description='Quantity required'),
        'rp_city': fields.String(required=True, description='Request person city'),
        'rp_pincode': fields.Integer(required=True, description='Request person pincode'),
        'rp_state': fields.String(required=True, description='Request person state'),
        'rp_phone': fields.String(required=True, description='Request person contact'),
        'comment': fields.String(required=False, description='Comments')
    })